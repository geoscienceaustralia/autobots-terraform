#Dockerfile for autobots-terraform

This repo is used to manage and deploy the autobots-terraform Docker image. This Docker image is used by Bitbucket Pipelines that deploy Terraform code.

Current version: Terraform 0.12.*
