FROM ubuntu:focal
MAINTAINER Zena Wolba

# General configuration and utilities
RUN apt-get update
RUN apt install -y software-properties-common
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y wget unzip jq curl git

# Python
RUN apt-get install -y python3-pip
RUN python3 --version

# Update pip3
RUN pip3 install --upgrade pip

# The following dependencies are necessary to avoid "failed building wheel for cryptography" errors
# This must be run before the following pip install command
RUN apt-get install -y build-essential libssl-dev libffi-dev python-dev
RUN pip3 install boto3 moto pyyaml adal aws-encryption-sdk-cli

# The following installs AWS CLI version 2. This is taken from official AWS documentation
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install

# Terraform
# Download Terraform - most recent version
RUN curl -o terraform.zip $(echo "https://releases.hashicorp.com/terraform/$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')/terraform_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/terraform | jq -r -M '.current_version')_linux_amd64.zip")

# Unzip and install
RUN unzip terraform.zip
RUN mv terraform /usr/local/bin/terraform
RUN terraform -v 

# Packer
# Download packer - most recent version
RUN curl -o packer.zip $(echo "https://releases.hashicorp.com/packer/$(curl -s https://checkpoint-api.hashicorp.com/v1/check/packer | jq -r -M '.current_version')/packer_$(curl -s https://checkpoint-api.hashicorp.com/v1/check/packer | jq -r -M '.current_version')_linux_amd64.zip")

# Unzip and install
RUN unzip packer.zip
RUN mv packer /usr/local/bin/packer
RUN packer -v
